const express       = require('express');
const bodyParser    = require('body-parser');

var app = express();

var urlencodedParser = bodyParser.urlencoded( {extend : false} );

app.use(bodyParser.json());

app.get('/', function (req, res) {
    res.send('Hello world!');
});

app.post('/', urlencodedParser, function(req, res) {
    res.send(req.body);
});

app.listen(3000);